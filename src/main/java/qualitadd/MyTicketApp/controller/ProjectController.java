package qualitadd.MyTicketApp.controller;

import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import qualitadd.MyTicketApp.Impservice.ImpProjectService;
import qualitadd.MyTicketApp.dto.ProjectDto;

import java.util.List;

@Controller
@RestController
@RequestMapping("")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class ProjectController {
    private final ImpProjectService impProjectService;

    public ProjectController(ImpProjectService impProjectService) {
        this.impProjectService = impProjectService;
    }

    @GetMapping("/project")
    @PreAuthorize("hasAnyRole('admin', 'user')")
    public List<ProjectDto> getListProject(@Valid @RequestParam String email) {
        List<ProjectDto> projectList = impProjectService.getProject(email);
        System.out.println("Résultat de la requête : " + projectList);
        return projectList;
    }
}
