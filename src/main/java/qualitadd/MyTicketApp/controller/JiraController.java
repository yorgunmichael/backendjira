package qualitadd.MyTicketApp.controller;

import com.atlassian.httpclient.api.HttpStatus;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.ProjectRestClient;
import com.atlassian.jira.rest.client.api.domain.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import qualitadd.MyTicketApp.Impservice.ImpJiraService;
import qualitadd.MyTicketApp.entites.Issues;

import java.io.IOException;
import java.util.*;

@RestController
@RequestMapping("/request/jira")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class JiraController {
    private final JiraRestClient jiraRestClient;
    private final ImpJiraService impJiraService;
    private final ObjectMapper objectMapper;


    public JiraController(JiraRestClient jiraRestClient, ImpJiraService impJiraService, ObjectMapper objectMapper) {
        this.jiraRestClient = jiraRestClient;
        this.impJiraService = impJiraService;
        this.objectMapper = objectMapper;
    }

//    @GetMapping("{issueKey}")
//    @PreAuthorize("hasAnyRole('admin','user')")
//    public ResponseEntity<String> getIssue(@PathVariable String issueKey) {
//        IssueRestClient issueClient = jiraRestClient.getIssueClient();
//
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.registerModule(new JodaModule());
//
//        try {
//            Issue issue = issueClient.getIssue(issueKey).claim();
//
//            String json = objectMapper.writeValueAsString(issue);
//            return ResponseEntity.ok(json);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.ordinal()).build();
//        }
//    }

//    @GetMapping("/projects")
//    public ResponseEntity<List<BasicProject>> getAllProjects() {
//        ProjectRestClient projectClient = jiraRestClient.getProjectClient();
//
//        try {
//            Iterable<BasicProject> projects = projectClient.getAllProjects().claim();
//            List<BasicProject> projectList = new ArrayList<>();
//            projects.forEach(projectList::add);
//
//            return ResponseEntity.ok(projectList);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.ordinal()).build();
//        }
//    }

    @GetMapping("/projects")
    @PreAuthorize("hasAnyRole('admin','user')")
    public ResponseEntity<List<BasicProject>> getAllProjectsForUser(@RequestParam("userEmail") String userEmail) {
        ProjectRestClient projectClient = jiraRestClient.getProjectClient();

        try {
            Iterable<BasicProject> allProjects = projectClient.getAllProjects().claim();
            System.out.println(allProjects);
            List<BasicProject> userProjects = new ArrayList<>();

            for (BasicProject basicProject : allProjects) {
                Project fullProject = projectClient.getProject(basicProject.getKey()).claim();

                String leadUserName = fullProject.getLead().getDisplayName();

                if (leadUserName.equalsIgnoreCase(userEmail)) {
                    userProjects.add(basicProject);
                }
            }
            return ResponseEntity.ok(userProjects);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.ordinal()).build();
        }
    }


//    @GetMapping("/tickets")
//    @PreAuthorize("hasAnyRole('admin','user')")
//    public ResponseEntity<List<Map<String, String>>> getTicketsByProject(@RequestParam String projectKey, @RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "1") Integer pageNumber) {
//        SearchRestClient searchClient = jiraRestClient.getSearchClient();
//        System.out.println("pageSize" + pageSize);
//        System.out.println("pageSize" + pageNumber);
//
//        String jqlQuery = "project = " + projectKey;
//
//        try {
//            int startIndex = (pageNumber - 1) * pageSize;
//
//            SearchResult searchResult = searchClient.searchJql(jqlQuery, pageSize, startIndex, null).claim();
//
//            List<Map<String, String>> ticketDetailsList = new ArrayList<>();
//
//            for (Issue issue : searchResult.getIssues()) {
//                Map<String, String> ticketDetails = new HashMap<>();
//                ticketDetails.put("key", issue.getKey());
//                ticketDetails.put("summary", issue.getSummary());
//                ticketDetails.put("description", issue.getDescription());
//
//                ticketDetailsList.add(ticketDetails);
//            }
//
//            return ResponseEntity.ok(ticketDetailsList);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.ordinal()).build();
//        }
//    }

//    @PostMapping("")
//    @PreAuthorize("hasAnyRole('admin','user')")
//    public ResponseEntity<Map<String, String>> createIssue(@RequestBody String requestBody) {
//        IssueRestClient issueClient = jiraRestClient.getIssueClient();
//        Map<String, String> response = new HashMap<>();
//
//        try {
//            ObjectMapper objectMapper = new ObjectMapper();
//            JsonNode jsonNode = objectMapper.readTree(requestBody);
//
//            IssueInputBuilder issueInputBuilder = new IssueInputBuilder();
//            if (jsonNode.has("fields")) {
//                JsonNode fieldsNode = jsonNode.get("fields");
//
//                if (fieldsNode.has("project") && fieldsNode.get("project").has("key")) {
//                    String projectKey = fieldsNode.get("project").get("key").asText();
//                    System.out.println(projectKey);
//                    issueInputBuilder.setProjectKey(projectKey);
//                }
//
//                if (fieldsNode.has("summary")) {
//                    String summary = fieldsNode.get("summary").asText();
//                    System.out.println(summary);
//                    issueInputBuilder.setSummary(summary);
//                }
//
//                if (fieldsNode.has("description")) {
//                    String description = fieldsNode.get("description").asText();
//                    System.out.println(description);
//                    issueInputBuilder.setDescription(description);
//                }
//
//                if (fieldsNode.has("issuetype") && fieldsNode.get("issuetype").has("id")) {
//                    Long issueTypeId = fieldsNode.get("issuetype").get("id").asLong();
//                    System.out.println(issueTypeId);
//                    issueInputBuilder.setIssueTypeId(issueTypeId);
//                }
//
//                if (fieldsNode.has("parent") && fieldsNode.get("parent").has("key")) {
//                    String parentKey = fieldsNode.get("parent").get("key").asText();
//                    System.out.println(parentKey);
//                    issueInputBuilder.setFieldValue("parent", ComplexIssueInputFieldValue.with("key", parentKey));
//                }
//            }
//
//            IssueInput newIssueInput = issueInputBuilder.build();
//            BasicIssue newIssue = issueClient.createIssue(newIssueInput).claim();
//            response.put("issueKey", newIssue.getKey());
//            return ResponseEntity.ok(response);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.ordinal()).body(response);
//        }
//    }


    @PostMapping("/createIssue")
    @PreAuthorize("hasAnyRole('admin','user')")
    public ResponseEntity<String> createIssue(@RequestPart("issues") String issuesJson, @RequestPart("pictures") List<MultipartFile> pictures) throws JSONException, IOException {

        Issues issues = objectMapper.readValue(issuesJson, Issues.class);
        issues.setPictures(pictures);

        String issueInput = impJiraService.createIssue(issues, pictures);
        JSONObject response = new JSONObject();
        response.put("issueKey", issueInput);

        return ResponseEntity.ok(response.toString());
    }


    @GetMapping("/listOfTicket")
    @PreAuthorize("hasAnyRole('admin','user')")
    public ResponseEntity<List<Issues>> getTicketsByProject(@RequestParam("projectKey") String projectName, @RequestParam("pageSize") Integer pageSize, @RequestParam("pageNumber") Integer pageNumber) {
        List<Issues> tickets = impJiraService.getListOfIssue(projectName, pageSize, pageNumber);
        return ResponseEntity.ok(tickets);
    }


    @DeleteMapping("/delete")
    @PreAuthorize("hasAnyRole('admin','user')")
    public void deleteJiraIssue(String issueKey) {
        impJiraService.deleteIssue(issueKey);
    }


//    @PutMapping("/update/{issueKey}")
//    @PreAuthorize("hasAnyRole('admin','user')")
//    public ResponseEntity<Map<String, String>> updateJiraIssue(@PathVariable String issueKey, @RequestBody Issues updatedIssues) {
//        impJiraService.updateIssue(issueKey, updatedIssues);
//        Map<String, String> response = new HashMap<>();
//        response.put("message", "Ticket updated successfully. Issue key: " + issueKey);
//        return ResponseEntity.ok(response);
//    }


    @PutMapping("/update/{issueKey}")
    @PreAuthorize("hasAnyRole('admin','user')")
    public ResponseEntity<Map<String, String>> updateJiraIssue(@PathVariable String issueKey, @RequestPart("issues") String updatedIssues, @RequestPart("pictures") List<MultipartFile> pictures) throws IOException {
        Issues issues = objectMapper.readValue(updatedIssues, Issues.class);
        issues.setPictures(pictures);

        impJiraService.updateIssue(issueKey, issues, pictures);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Ticket updated successfully. Issue key: " + issueKey);
        return ResponseEntity.ok(response);
    }


    @GetMapping("/history/{issueKey}")
    public List<ChangelogGroup> getIssueChangelog(@PathVariable String issueKey) {
        System.out.println("nous sommes bien ici");
        System.out.println("nous sommes bien ici" + impJiraService.getIssueChangelog(issueKey));
        return impJiraService.getIssueChangelog(issueKey);
    }

    @GetMapping("/listOfTickets")
    @PreAuthorize("hasAnyRole('admin','user')")
    public ResponseEntity<List<Issues>> getTicketsByProjects(@RequestParam("projectKey") String projectName) {
        List<Issues> tickets = impJiraService.getListOfIssue(projectName);
        return ResponseEntity.ok(tickets);
    }


//    @GetMapping("/issuetypes")
//    public ResponseEntity<List<IssueType>> getAllIssueTypes() {
//        MetadataRestClient metadataClient = jiraRestClient.getMetadataClient();
//
//        try {
//            Iterable<IssueType> issueTypes = metadataClient.getIssueTypes().claim();
//            List<IssueType> issueTypeList = new ArrayList<>();
//            issueTypes.forEach(issueTypeList::add);
//
//            return ResponseEntity.ok(issueTypeList);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.ordinal()).build();
//        }
//    }

//    public ResponseEntity<String> updateIssueJira(@PathVariable String issueKey, @RequestBody String requestBody) {
//        IssueRestClient issueClient = jiraRestClient.getIssueClient();
//
//        try {
//            Issue issue = issueClient.getIssue(issueKey).claim();
//
//            ObjectMapper objectMapper = new ObjectMapper();
//            JsonNode jsonNode = objectMapper.readTree(requestBody);
//
//            IssueInputBuilder issueInputBuilder = new IssueInputBuilder();
//
//            if (jsonNode.has("fields")) {
//                JsonNode fieldsNode = jsonNode.get("fields");
//
//                if (fieldsNode.has("summary")) {
//                    String summary = fieldsNode.get("summary").asText();
//                    issueInputBuilder.setSummary(summary);
//                }
//
//                if (fieldsNode.has("description")) {
//                    String description = fieldsNode.get("description").asText();
//                    issueInputBuilder.setDescription(description);
//                }
//            }
//
//            IssueInput updatedIssueInput = issueInputBuilder.build();
//            issueClient.updateIssue(issue.getKey(), updatedIssueInput).claim();
//
//            return ResponseEntity.ok("Issue updated successfully.");
//        } catch (Exception e) {
//            e.printStackTrace();
//            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.ordinal()).build();
//        }
//    }


//    @PreAuthorize("hasAnyRole('admin','user')")
//    public void updateIssueDescription(@RequestParam String issueKey, @RequestParam String newDescription) {
//        IssueInput newIssue = new IssueInputBuilder().setDescription(newDescription).build();
//        IssueRestClient issueClient = jiraRestClient.getIssueClient();
//        issueClient.updateIssue(issueKey, newIssue).claim();
//    }
//
//    @DeleteMapping("")
//    @PreAuthorize("hasAnyRole('admin','user')")
//    public void deleteIssue(String issueKey, boolean deleteSubtasks) {
//        IssueRestClient issueClient = jiraRestClient.getIssueClient();
//        System.out.println(issueClient);
//        issueClient.deleteIssue(issueKey, deleteSubtasks).claim();
//    }
}
