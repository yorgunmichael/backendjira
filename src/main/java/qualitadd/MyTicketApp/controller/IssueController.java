package qualitadd.MyTicketApp.controller;

import jakarta.validation.constraints.Size;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import qualitadd.MyTicketApp.Impservice.ImpIssueService;
import qualitadd.MyTicketApp.dto.IssueDto;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController

//@RequestMapping(value = "", method = {RequestMethod.GET, RequestMethod.PUT})

@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class IssueController {

    private final ImpIssueService impIssueService;


    public IssueController(ImpIssueService impIssueService) {
        this.impIssueService = impIssueService;
    }

    @PostMapping("/addIssue")
    @PreAuthorize("hasAnyRole('user','admin')")
    public void createIssue(@RequestParam("title") String title, @RequestParam("description") @Size(max = 500, message = "La description doit pas dépasser 10 caractères") String description, @RequestParam("types") String types, @RequestParam("priority") String priority, @RequestParam("date_create") String date_create, @RequestParam("status") String status, @RequestParam("picture") MultipartFile picture, @RequestParam("url") String url, @RequestParam("name_project") String name_project, @RequestParam("env") String env) throws ParseException, IOException {
        System.out.println("date dans méthode crear" + date_create);
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date receiveDate = inputFormat.parse(String.valueOf(date_create));
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = outputFormat.format(receiveDate);
        System.out.println("nouve Date: " + formattedDate);
        impIssueService.createIssue(title, description, types, priority, formattedDate, status, picture.getBytes(), url, name_project, env);
        System.out.println("Nous venons de créer un ticket ");
    }

//    @GetMapping("/admin")
//    public ResponseEntity<String> getAdmin(Principal principal) {
//        JwtAuthenticationToken token = (JwtAuthenticationToken) principal;
//        String userName = (String) token.getTokenAttributes().get("name");
//        String userEmail = (String) token.getTokenAttributes().get("email");
//        return ResponseEntity.ok("Hello Admin \nUser Name : " + userName + "\nUser Email : " + userEmail);
//    }

    @GetMapping("/valueProject")
    @PreAuthorize("hasAnyRole('admin','user')")
    public ResponseEntity<Page<IssueDto>> getIssuesByProject(@RequestParam("nameProject") String nameProject, @RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "0") Integer pageNumber) {
        System.out.println("name:" + nameProject);
        System.out.println("pageSize: " + pageSize);
        System.out.println("pageNumber: " + pageNumber);


        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by("title").ascending());

        List<IssueDto> issues = impIssueService.getIssueByProject(nameProject, pageable).getContent();
        System.out.println(issues);
        Page<IssueDto> issuePage = new PageImpl<>(issues, pageable, issues.size());
        return ResponseEntity.ok(issuePage);
    }

    @GetMapping("/searchIssue")
    @PreAuthorize("hasAnyRole('admin','user')")
    public ResponseEntity<Page<IssueDto>> searchIssueByKeyword(@RequestParam("nameProject") String nameProject, @RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "0") Integer pageNumber, @RequestParam("keyword") String keyword) {
        System.out.println("name:" + nameProject);
        System.out.println("pageSize: " + pageSize);
        System.out.println("pageNumber: " + pageNumber);
        System.out.println("keyword: " + keyword);
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by("title").ascending());
        List<IssueDto> issues = impIssueService.getIssueByKeyword(nameProject, pageable, keyword).getContent();
        System.out.println(issues);
        Page<IssueDto> issuePage = new PageImpl<>(issues, pageable, issues.size());
        return ResponseEntity.ok(issuePage);
    }

    @GetMapping("/filterIssue")
    @PreAuthorize("hasAnyRole('admin', 'user')")
    public ResponseEntity<Page<IssueDto>> filterIssueByStatus(@RequestParam("nameProject") String nameProject, @RequestParam("statuspos1") String statuspos1, @RequestParam("statuspos2") String statuspos2, @RequestParam(defaultValue = "5") Integer pageSize, @RequestParam(defaultValue = "0") Integer pageNumber) {
        System.out.println("name:" + nameProject);
        System.out.println("pageSize: " + pageSize);
        System.out.println("pageNumber: " + pageNumber);
        System.out.println("statuspos1: " + statuspos1);
        System.out.println("statuspos2: " + statuspos2);
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by("title").ascending());
        List<IssueDto> issues = impIssueService.getIssueByStatus(nameProject, statuspos1, statuspos2, pageable).getContent();
        System.out.println(issues);
        Page<IssueDto> issuePage = new PageImpl<>(issues, pageable, issues.size());
        return ResponseEntity.ok(issuePage);
    }

    @PutMapping("/{issueId}")
    @PreAuthorize("hasAnyRole('admin')")
    public void handleUpdateIssue(@PathVariable("issueId") Integer issueId, @RequestParam("title") String title, @RequestParam("description") @Size(max = 500, message = "La description doit pas dépasser 10 caractères") String description, @RequestParam("types") String types, @RequestParam("priority") String priority, @RequestParam("date_create") String date_create, @RequestParam("status") String status, @RequestParam("picture") MultipartFile picture, @RequestParam("url") String url, @RequestParam("name_project") String name_project, @RequestParam("env") String env) throws ParseException, IOException {
        System.out.println("update ticket debut");
        System.out.println("Date: " + date_create);
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        Date receiveDate = inputFormat.parse(String.valueOf(date_create));
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = outputFormat.format(receiveDate);
        System.out.println("nouvelle date mise à jour: " + formattedDate);
        impIssueService.updateIssue(issueId, title, description, types, priority, formattedDate, status, picture.getBytes(), url, name_project, env);
        System.out.println("Le ticket à été mise à jour avec succès");
        System.out.println("update ticket fin");
    }

    @DeleteMapping("/delete/{issueId}")
    @PreAuthorize("hasAnyRole('admin')")
    public void deleteIssueById(@PathVariable("issueId") Integer issueId) {
        impIssueService.deleteIssueById(issueId);
    }
}


