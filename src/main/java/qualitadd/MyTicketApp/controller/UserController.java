package qualitadd.MyTicketApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import qualitadd.MyTicketApp.Impservice.ImpUserService;
import qualitadd.MyTicketApp.dto.UserDto;


@Controller
@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class UserController {

    private final ImpUserService impUserService;

    public UserController(ImpUserService impUserService) {
        this.impUserService = impUserService;
    }

    @PostMapping("/login")
    @PreAuthorize("hasAnyRole('admin')")
    public ResponseEntity<UserDto> login(@RequestParam String email, @RequestParam String password) {
        System.out.println(email + ' ' + password);
        UserDto userDto = impUserService.validateCredentials(email, password);
        System.out.println(userDto.getEmail());
        System.out.println("/login ========+>>>>>> " + userDto);
        return ResponseEntity.ok(userDto);
    }


    @PostMapping("/addUser")
    @PreAuthorize("hasAnyAuthority('SCOPE_profile','SCOPE_infouser')")
    public UserDto saveUser(@RequestBody UserDto userDto) {
        return impUserService.saveUser(userDto);
    }
}