package qualitadd.MyTicketApp.Iservice;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import qualitadd.MyTicketApp.dto.IssueDto;

public interface IssueService {
    void createIssue(String title, String description, String types, String priority, String date_create, String status, byte[] picture, String url, String name_project, String env);
    Page<IssueDto> getIssueByProject(String nameProject, Pageable pageable);
    Page<IssueDto> getIssueByKeyword(String nameProject, Pageable pageable, String keyword);
    void deleteIssueById(Integer issue_id);
    void updateIssue(Integer issueId, String title, String description, String types, String priority, String date_create, String status, byte[] picture, String url, String name_project, String env);
    Page<IssueDto> getIssueByStatus(String nameProject, String statuspos1, String statuspos2, Pageable pageable);
}


