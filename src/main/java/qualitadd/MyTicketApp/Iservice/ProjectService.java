package qualitadd.MyTicketApp.Iservice;

import qualitadd.MyTicketApp.dto.ProjectDto;

import java.util.List;

public interface ProjectService {
    List<ProjectDto> getProject(String email);
}
