package qualitadd.MyTicketApp.Iservice;

import qualitadd.MyTicketApp.dto.UserDto;

public interface UserService {
    UserDto validateCredentials(String email, String password);
    UserDto saveUser(UserDto userDto);






}
