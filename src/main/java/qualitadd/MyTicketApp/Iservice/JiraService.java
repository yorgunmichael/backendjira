package qualitadd.MyTicketApp.Iservice;

import com.atlassian.jira.rest.client.api.domain.ChangelogGroup;
import com.atlassian.jira.rest.client.api.domain.IssueType;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.codehaus.jettison.json.JSONException;
import org.springframework.web.multipart.MultipartFile;
import qualitadd.MyTicketApp.entites.Issues;

import java.io.IOException;
import java.util.List;

public interface JiraService {
     String mapToJiraIssue(Issues issues, IssueType issueType, List<MultipartFile> pictures) throws JSONException, IOException;

    String createIssue(Issues issues, List<MultipartFile> pictures) throws JSONException, IOException;

    IssueType getIssueTypeByName(String name);

    void deleteIssue(String issueKey);

    void updateIssue(String issueKey, Issues issues, List<MultipartFile> pictures) throws IOException;

    List<ChangelogGroup> getIssueChangelog(String issueKey) throws JsonProcessingException;

}
