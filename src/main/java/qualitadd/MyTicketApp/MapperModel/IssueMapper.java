package qualitadd.MyTicketApp.MapperModel;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import qualitadd.MyTicketApp.dto.IssueDto;
import qualitadd.MyTicketApp.entites.Issue;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.FIELD)
public interface IssueMapper {
    IssueDto issueToIssueDto(Issue issue);
    Issue issueDtoToIssue(IssueDto issueDto);
}
