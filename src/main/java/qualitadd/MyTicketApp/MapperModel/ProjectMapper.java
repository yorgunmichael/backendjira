package qualitadd.MyTicketApp.MapperModel;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import qualitadd.MyTicketApp.dto.ProjectDto;
import qualitadd.MyTicketApp.entites.Project;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.FIELD)
public interface ProjectMapper {
    ProjectDto projecttoDto(Project project);
    Project DtotoProject(ProjectDto projectDto);
}
