package qualitadd.MyTicketApp.MapperModel;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import qualitadd.MyTicketApp.dto.ProfileDto;
import qualitadd.MyTicketApp.entites.Profile;


@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.FIELD)
public interface ProfilMapper {
    ProfileDto profilToProfileDto(Profile profile);
    Profile profilDtoToProfil(ProfileDto profileDto);
}
