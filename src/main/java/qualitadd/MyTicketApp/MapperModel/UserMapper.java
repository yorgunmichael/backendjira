package qualitadd.MyTicketApp.MapperModel;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import qualitadd.MyTicketApp.dto.UserDto;
import qualitadd.MyTicketApp.entites.User;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.FIELD)
public interface UserMapper {
    UserDto usertoDto(User user);
    User DtotoUser(UserDto userDto);

}
