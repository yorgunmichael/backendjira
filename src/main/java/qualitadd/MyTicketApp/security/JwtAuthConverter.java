//package qualitadd.MyTicketApp.security;
//
//import org.springframework.core.convert.converter.Converter;
//import org.springframework.security.authentication.AbstractAuthenticationToken;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.oauth2.jwt.Jwt;
//import org.springframework.security.oauth2.jwt.JwtClaimNames;
//import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
//import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
//import org.springframework.stereotype.Component;
//
//import java.util.Collection;
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//@Component
//public class JwtAuthConverter implements Converter<Jwt, AbstractAuthenticationToken> {
//
//    private final JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
//    private final JwtAuthConverterProperties properties;
//
//    public JwtAuthConverter(JwtAuthConverterProperties properties) {
//        this.properties = properties;
//    }
//
//    @Override
//    public AbstractAuthenticationToken convert(Jwt jwt) {
//        Collection<GrantedAuthority> authorities = Stream.concat(
//                jwtGrantedAuthoritiesConverter.convert(jwt).stream(),
//                extractResourceRoles(jwt).stream()).collect(Collectors.toSet());
//        logRoles(authorities);
//
//        return new JwtAuthenticationToken(jwt, authorities, getPrincipalClaimName(jwt));
//    }
//
//    private void logRoles(Collection<GrantedAuthority> authorities) {
//        Set<String> roles = authorities.stream()
//                .map(GrantedAuthority::getAuthority)
//                .collect(Collectors.toSet());
//        String rolesString = String.join(", ", roles);
//        System.out.println("Roles: " + rolesString);
//    }
//
//
//    private String getPrincipalClaimName(Jwt jwt) {
//        String claimName = JwtClaimNames.SUB;
//        if (properties.getPrincipalAttribute() != null) {
//            claimName = properties.getPrincipalAttribute();
//        }
//        System.out.println(claimName);
//        return jwt.getClaim(claimName);
//    }
//
//    private Collection<? extends GrantedAuthority> extractResourceRoles(Jwt jwt) {
//        Collection<String> roles = new HashSet<>();
//
//        // Extraction des rôles de "realm_access"
//        Map<String, Object> realmAccess = jwt.getClaim("realm_access");
//        if (realmAccess != null) {
//            Collection<String> realmRoles = (Collection<String>) realmAccess.get("roles");
//            if (realmRoles != null) {
//                roles.addAll(realmRoles);
//            }
//        }
//
//        // Extraction des rôles de "resource_access"
//        Map<String, Object> resourceAccess = jwt.getClaim("resource_access");
//        if (resourceAccess != null) {
//            Map<String, Object> appBackendResource = (Map<String, Object>) resourceAccess.get("app-backend");
//            if (appBackendResource != null) {
//                Collection<String> appBackendRoles = (Collection<String>) appBackendResource.get("roles");
//                if (appBackendRoles != null) {
//                    roles.addAll(appBackendRoles);
//                }
//            }
//        }
//
//        return roles.stream()
//                .map(role -> new SimpleGrantedAuthority("ROLE_" + role))
//                .collect(Collectors.toSet());
//    }
//
//}
