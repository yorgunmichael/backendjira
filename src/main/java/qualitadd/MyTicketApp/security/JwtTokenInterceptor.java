//package qualitadd.MyTicketApp.security;
//
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpRequest;
//import org.springframework.http.client.ClientHttpRequestExecution;
//import org.springframework.http.client.ClientHttpRequestInterceptor;
//import org.springframework.http.client.ClientHttpResponse;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.oauth2.jwt.Jwt;
//import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
//import org.springframework.util.StringUtils;
//
//import java.io.IOException;
//
//public class JwtTokenInterceptor implements ClientHttpRequestInterceptor {
//
//    private static final String AUTHORIZATION_HEADER = "Authorization";
//    private static final String BEARER_PREFIX = "Bearer ";
//    @Override
//    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        if (authentication instanceof JwtAuthenticationToken) {
//            JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;
//            Jwt jwt = jwtAuthenticationToken.getToken();
//            String token = jwt.getTokenValue();
//            System.out.println("Token:" +token);
//            request.getHeaders().add(AUTHORIZATION_HEADER, BEARER_PREFIX + token);
//            System.out.println(AUTHORIZATION_HEADER +","+ BEARER_PREFIX + token);
//            System.out.println("fin de interceptor");
//        }
//        return execution.execute(request, body);
//    }
//}
