//package qualitadd.MyTicketApp.Impservice;
//
//import jakarta.transaction.Transactional;
//import org.springframework.stereotype.Service;
//import qualitadd.MyTicketApp.entites.BugsIssue;
//import qualitadd.MyTicketApp.repository.BugsIssueRepository;
//
//@Service
//public class BugsIssueService {
//
//    private final BugsIssueRepository bugsIssueRepository;
//
//    public BugsIssueService(BugsIssueRepository bugsIssueRepository) {
//        this.bugsIssueRepository = bugsIssueRepository;
//    }
//
//    @Transactional
//    public void createBugsIssue() {
//        BugsIssue bugIssue = new BugsIssue();
//        bugIssue.setIssueKey("BUG-003");
//        bugIssue.setReporter("John Doe");
//        bugIssue.setSummary("Bug summary");
//
//        // Attributs spécifiques à BugsIssue accessibles via les méthodes générées par Lombok
//        bugIssue.setLevel("High");
//        bugIssue.setDetectedBy("Ali Bousenna");
//        bugIssue.setRequestFormat("Bug report format");
//
//        bugsIssueRepository.save(bugIssue);
//    }
//}
