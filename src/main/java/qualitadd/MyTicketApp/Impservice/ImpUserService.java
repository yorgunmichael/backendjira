package qualitadd.MyTicketApp.Impservice;

import jakarta.transaction.Transactional;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import qualitadd.MyTicketApp.Iservice.UserService;
import qualitadd.MyTicketApp.MapperModel.ProfilMapper;
import qualitadd.MyTicketApp.MapperModel.UserMapper;
import qualitadd.MyTicketApp.dto.ProfileDto;
import qualitadd.MyTicketApp.dto.UserDto;
import qualitadd.MyTicketApp.entites.Profile;
import qualitadd.MyTicketApp.entites.User;
import qualitadd.MyTicketApp.repository.ProfileRepository;
import qualitadd.MyTicketApp.repository.UserRepository;



@Service
public class ImpUserService implements UserService {
    private final UserRepository userRepository;
    private final ProfileRepository profileRepository;
    private final UserMapper userMapper;
    private final ProfilMapper profilMapper;


    public ImpUserService(UserRepository userRepository, ProfileRepository profileRepository, UserMapper userMapper, ProfilMapper profilMapper) {
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
        this.userMapper = userMapper;
        this.profilMapper = profilMapper;
    }


    @Override
    public UserDto validateCredentials(String email, String password) {
        User user = userRepository.findByEmailAndPassword(email, password);

        if (user == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }

        UserDto userDto = userMapper.usertoDto(user);
        Profile profile = user.getProfile();
        ProfileDto profileDto = profilMapper.profilToProfileDto(profile);
        userDto.setProfileDto(profileDto);

        return userDto;
    }

    @Transactional
    @Override
    public UserDto saveUser(UserDto userDto) {
        User user  = userMapper.DtotoUser(userDto);
        Profile profile = profilMapper.profilDtoToProfil(userDto.getProfileDto());
        profileRepository.save(profile);
        user.setProfile(profile);
        userDto.setProfileDto(profilMapper.profilToProfileDto(profile));
        return userMapper.usertoDto(userRepository.save(user));
    }
}