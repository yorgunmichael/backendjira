package qualitadd.MyTicketApp.Impservice;

import jakarta.transaction.Transactional;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import qualitadd.MyTicketApp.Iservice.IssueService;
import qualitadd.MyTicketApp.dto.IssueDto;
import qualitadd.MyTicketApp.entites.Issue;
import qualitadd.MyTicketApp.repository.IssueRepository;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@Service
public class ImpIssueService implements IssueService {
    private final IssueRepository issueRepository;

    public ImpIssueService(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    @Transactional
    @Override
    public void createIssue(String title, String description, String types, String priority, String date_create, String status, byte[] picture, String url, String name_project, String env) {
        Issue issue = new Issue();
        issue.setTitle(title);
        issue.setDescription(description);
        issue.setIssueType(types);
        issue.setPriority(priority);
        issue.setDate_creation(date_create);
        issue.setStatus(status);
        issue.setPicture(picture);
        issue.setUrl(url);
        issue.setProjectName(name_project);
        issue.setIssueType(env);
        issueRepository.createIssue(title, description, types, priority, date_create, status, Arrays.toString(picture), url, name_project, env);
        System.out.println("issue sauvegardé!");
    }

    @Override
    public Page<IssueDto> getIssueByProject(String nameProject, Pageable pageable) {
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("title").ascending());
        Page<Object[]> issuePage = issueRepository.findByProject(nameProject, pageable);

        System.out.println("contenue: " + issuePage.getContent());
        System.out.println("Liste des issuees 1:" + issuePage);


        List<IssueDto> issueDtos = new ArrayList<>();
        List<Object[]> issueObjects = issuePage.getContent();

        for (Object[] issue : issueObjects) {
            IssueDto issueDto = new IssueDto();
            issueDto.setIssue_id((Integer) issue[0]);
            issueDto.setTitle((String) issue[1]);
            issueDto.setDescription((String) issue[2]);
            issueDto.setStatus((String) issue[3]);
            issueDto.setPriority((String) issue[4]);
            issueDto.setIssueType((String) issue[5]);
            String dateString = (String) issue[6];
            DateFormat dateFormated = new SimpleDateFormat("yyyy-MM-dd");

            try {
                Date date = dateFormated.parse(dateString);
                issueDto.setDate_creation(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            issueDto.setUrl((String) issue[7]);
            issueDto.setProjectName((String) issue[8]);
            issueDto.setEnv((String) issue[9]);

            issueDtos.add(issueDto);
        }

        System.out.println("Liste des issuees 2:" + issueDtos);
        return new PageImpl<>(issueDtos, pageable, issuePage.getTotalElements());
    }

    @Override
    public Page<IssueDto> getIssueByKeyword(String nameProject, Pageable pageable, String keyword) {
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("title").ascending());
        Page<Object[]> issuePage = issueRepository.searchIssueByKeyword(nameProject, pageable, keyword);

        List<IssueDto> issueDtos = new ArrayList<>();
        List<Object[]> issueObjects = issuePage.getContent();

        for (Object[] issue : issueObjects) {
            IssueDto issueDto = new IssueDto();
            issueDto.setIssue_id((Integer) issue[0]);
            issueDto.setTitle((String) issue[1]);
            issueDto.setDescription((String) issue[2]);
            issueDto.setStatus((String) issue[3]);
            issueDto.setPriority((String) issue[4]);
            issueDto.setIssueType((String) issue[5]);
            String dateString = (String) issue[6];
            DateFormat dateFormated = new SimpleDateFormat("yyyy-MM-dd");

            try {
                Date date = dateFormated.parse(dateString);
                issueDto.setDate_creation(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            issueDto.setUrl((String) issue[7]);
            issueDto.setProjectName((String) issue[8]);
            issueDto.setEnv((String) issue[9]);

            issueDtos.add(issueDto);
        }

        return new PageImpl<>(issueDtos, pageable, issuePage.getTotalElements());
    }


    @Override
    public void deleteIssueById(Integer issue_id) {
        issueRepository.deleteIssueById(issue_id);
    }

    @Override
    public void updateIssue(Integer issueId, String title, String description, String types, String priority, String date_create, String status, byte[] picture, String url, String name_project, String env) {
        Issue existingIssue = issueRepository.findById(issueId).orElse(null);

        if (existingIssue != null) {
            existingIssue.setTitle(title);
            existingIssue.setDescription(description);
            existingIssue.setIssueType(types);
            existingIssue.setDate_creation(date_create);
            existingIssue.setStatus(status);
            existingIssue.setPicture(picture);
            existingIssue.setUrl(url);
            existingIssue.setProjectName(name_project);
            System.out.println("je suis a ce niveau de la mise à jour");
            issueRepository.updateIssue(issueId, title, description, types, priority, date_create, status, picture, url, name_project, env);
            System.out.println("fin maj");
        }

    }

    @Override
    public Page<IssueDto> getIssueByStatus(String nameProject, String statuspos1, String statuspos2, Pageable pageable) {
        pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.by("title").ascending());
        Page<Object[]> issuepage = issueRepository.filterIssueByStatut(nameProject, statuspos1, statuspos2, pageable);

        List<IssueDto> issueDtos = new ArrayList<>();
        List<Object[]> issueObjects = issuepage.getContent();

        for (Object[] issue : issueObjects) {
            IssueDto issueDto = new IssueDto();
            issueDto.setIssue_id((Integer) issue[0]);
            issueDto.setTitle((String) issue[1]);
            issueDto.setDescription((String) issue[2]);
            issueDto.setStatus((String) issue[3]);
            issueDto.setPriority((String) issue[4]);
            issueDto.setIssueType((String) issue[5]);
            String dateString = (String) issue[6];
            DateFormat dateFormated = new SimpleDateFormat("yyyy-MM-dd");

            try {
                Date date = dateFormated.parse(dateString);
                issueDto.setDate_creation(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            issueDto.setUrl((String) issue[7]);
            issueDto.setProjectName((String) issue[8]);
            issueDto.setEnv((String) issue[9]);

            issueDtos.add(issueDto);
        }
        return new PageImpl<>(issueDtos, pageable, issuepage.getTotalElements());
    }
}


