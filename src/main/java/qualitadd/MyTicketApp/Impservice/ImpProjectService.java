package qualitadd.MyTicketApp.Impservice;

import org.springframework.stereotype.Service;
import qualitadd.MyTicketApp.Iservice.ProjectService;
import qualitadd.MyTicketApp.MapperModel.ProjectMapper;
import qualitadd.MyTicketApp.MapperModel.UserMapper;
import qualitadd.MyTicketApp.dto.ProjectDto;
import qualitadd.MyTicketApp.dto.UserDto;
import qualitadd.MyTicketApp.entites.Project;
import qualitadd.MyTicketApp.entites.User;
import qualitadd.MyTicketApp.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImpProjectService implements ProjectService {
    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;
    private final UserMapper userMapper;
    public ImpProjectService(ProjectRepository projectRepository, ProjectMapper projectMapper, UserMapper userMapper) {
        this.projectRepository = projectRepository;
        this.projectMapper = projectMapper;
        this.userMapper = userMapper;
    }

    @Override
    public List<ProjectDto> getProject(String email) {
        List<Project> projectList = projectRepository.findProjectByEmails(email);
        List<ProjectDto> projecDtoList = new ArrayList<>();
        for(Project project: projectList) {
            ProjectDto projectDto = projectMapper.projecttoDto(project);
            User user = project.getUser();
            if(user != null) {
                UserDto userDto = userMapper.usertoDto(user);
                projectDto.setUser(userDto);
            }
            projecDtoList.add(projectDto);
        }
        return projecDtoList;
    }
}
