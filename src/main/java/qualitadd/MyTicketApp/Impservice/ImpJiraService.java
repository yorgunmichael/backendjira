package qualitadd.MyTicketApp.Impservice;

import com.atlassian.jira.rest.client.api.IssueRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.*;
import com.atlassian.jira.rest.client.api.domain.input.ComplexIssueInputFieldValue;
import com.atlassian.jira.rest.client.api.domain.input.IssueInput;
import com.atlassian.jira.rest.client.api.domain.input.IssueInputBuilder;
import com.atlassian.jira.rest.client.api.domain.input.LinkIssuesInput;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.atlassian.util.concurrent.Promise;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import qualitadd.MyTicketApp.Iservice.JiraService;
import qualitadd.MyTicketApp.entites.BugsIssue;
import qualitadd.MyTicketApp.entites.DeployIssue;
import qualitadd.MyTicketApp.entites.EvolutionIssue;
import qualitadd.MyTicketApp.entites.Issues;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ImpJiraService implements JiraService {

    private final JiraRestClient jiraRestClient;

    public ImpJiraService(JiraRestClient jiraRestClient) {
        this.jiraRestClient = jiraRestClient;
    }


    @Override
    public String createIssue(Issues issues, List<MultipartFile> pictures) throws IOException {
        System.out.println("picturespictures==> " + pictures);
        IssueType issueType = getIssueTypeByName(issues.getIssueType());
        System.out.println("issueType: " + issueType);
        return mapToJiraIssue(issues, issueType, pictures);
    }

    private void linkIssues(BasicIssue sourceIssue, String targetIssueKey, String relationType) {
        if (targetIssueKey != null && !targetIssueKey.isEmpty()) {
            LinkIssuesInput linkIssuesInput = new LinkIssuesInput(
                    sourceIssue.getKey(),
                    targetIssueKey,
                    relationType
            );
            jiraRestClient.getIssueClient().linkIssue(linkIssuesInput).claim();
        }
    }


    @Override
    public String mapToJiraIssue(Issues issues, IssueType issueType, List<MultipartFile> pictures) throws IOException {
        IssueInputBuilder inputBuilder = new IssueInputBuilder();
        setCommunIssueFiels(inputBuilder, issues);

        if (issues instanceof BugsIssue) {
            setBugsIssueFields(inputBuilder, (BugsIssue) issues);

        } else if (issues instanceof DeployIssue) {
            setDeployIssueFields(inputBuilder, (DeployIssue) issues);

        } else if (issues instanceof EvolutionIssue) {
            setEvolutionIssueFields(inputBuilder, (EvolutionIssue) issues);

        }
        inputBuilder.setIssueType(issueType);

        IssueInput issueInput = inputBuilder.build();
        Promise<BasicIssue> promise = jiraRestClient.getIssueClient().createIssue(issueInput);
        BasicIssue basicIssue = promise.claim();

        linkIssues(basicIssue, issues.getIssueLinked(), "Relates");

        String jiraBaseUrl = "https://michaelchacha.atlassian.net";
        URI attachmentUri = URI.create(jiraBaseUrl + "/rest/api/2/issue/" + basicIssue.getKey() + "/attachments");

        for (MultipartFile picture : pictures) {
            if (picture != null) {
                InputStream pictureStream = picture.getInputStream();
                jiraRestClient.getIssueClient().addAttachment(attachmentUri, pictureStream, picture.getOriginalFilename());
            }
        }

        System.out.println("BASIC KEY ==> " + basicIssue.getKey());

        return basicIssue.getKey();
    }


    public IssueType getIssueTypeByName(String name) {
        Iterable<IssueType> issueTypes = jiraRestClient.getMetadataClient().getIssueTypes().claim();
        for (IssueType issueType : issueTypes) {
            if (name.equals(issueType.getName())) {
                return issueType;
            }
        }
        throw new IllegalArgumentException("IssueType not found: " + name);
    }

    private void setCommunIssueFiels(IssueInputBuilder inputBuilder, Issues issues) {
        inputBuilder.setProjectKey(issues.getProjectKey());
        inputBuilder.setSummary(issues.getSummary());
        inputBuilder.setDescription(issues.getDescription());


        // Pour le champs Responsable Produit
        ComplexIssueInputFieldValue managerProduitFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", issues.getManagerProduct())
        );
        inputBuilder.setFieldValue("customfield_10065", managerProduitFieldValue);

        // Pour le champs Responsable R&D
        ComplexIssueInputFieldValue managerRDFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", issues.getManagerRD())
        );
        inputBuilder.setFieldValue("customfield_10066", managerRDFieldValue);

        // Pour le champs Collaborateur
        ComplexIssueInputFieldValue collaboratorFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", issues.getCollaborator())
        );
        inputBuilder.setFieldValue("customfield_10052", collaboratorFieldValue);

        // Pour le champs EnvironnementUrl
        inputBuilder.setFieldValue("customfield_10038", issues.getEnvironmentUrl());

        // Pour le champs Commentaire Recette KO
        inputBuilder.setFieldValue("customfield_10076", issues.getCommentRecipeKO());


        inputBuilder.setFieldValue("customfield_10078", issues.getDateOfRequest());
        inputBuilder.setFieldValue("customfield_10072", issues.getDeadlineIntern());
        inputBuilder.setFieldValue("customfield_10073", issues.getDeadlineCustomer());
        inputBuilder.setFieldValue("customfield_10074", issues.getDateOfRealization());
        inputBuilder.setFieldValue("customfield_10075", issues.getDateOfDelivery());
        inputBuilder.setFieldValue("customfield_10050", issues.getComments());
        inputBuilder.setFieldValue("customfield_10077", issues.getInterestsCollateral());

    }

    private void setBugsIssueFields(IssueInputBuilder inputBuilder, BugsIssue bugsIssue) {
        System.out.println("nous sommes dans bug");
        // Pour le champs DetectedBy
        ComplexIssueInputFieldValue detectedByFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", bugsIssue.getDetectedBy())
        );
        inputBuilder.setFieldValue("customfield_10041", detectedByFieldValue);


        // Pour le champs Niveau
        ComplexIssueInputFieldValue levelByFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", bugsIssue.getLevel())
        );
        inputBuilder.setFieldValue("customfield_10079", Arrays.asList(levelByFieldValue));


        // Pour le champs Format de la demande
        ComplexIssueInputFieldValue formatRequestByFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", bugsIssue.getRequestFormat())
        );
        inputBuilder.setFieldValue("customfield_10054", Arrays.asList(formatRequestByFieldValue));

    }

    private void setDeployIssueFields(IssueInputBuilder inputBuilder, DeployIssue deployIssue) {
        System.out.println("nous sommes dans Deploy");

        // Pour le champs Nature
        ComplexIssueInputFieldValue natureFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", deployIssue.getNature())
        );
        inputBuilder.setFieldValue("customfield_10056", Arrays.asList(natureFieldValue));

        // Pour le champs Prise en compte de l'évolution
        ComplexIssueInputFieldValue recognitionEvolutionFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", deployIssue.getRecognitionEvolution())
        );
        inputBuilder.setFieldValue("customfield_10080", Arrays.asList(recognitionEvolutionFieldValue));


        // Pour le champs Priorité
        ComplexIssueInputFieldValue priorityFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", deployIssue.getPriority())
        );
        inputBuilder.setFieldValue("customfield_10058", Arrays.asList(priorityFieldValue));


        // Pour le champs Reporter
        ComplexIssueInputFieldValue reporterFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", deployIssue.getReporter())
        );
        inputBuilder.setFieldValue("reporter", reporterFieldValue);


        // Pour le champs DetectedBy
        ComplexIssueInputFieldValue detectedByFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", deployIssue.getDetectedBy())
        );
        inputBuilder.setFieldValue("customfield_10041", detectedByFieldValue);

        // Pour le champs estimatePure
        inputBuilder.setFieldValue("customfield_10055", deployIssue.getEstimatePure());

        // Pour le champs estimateTotal
        inputBuilder.setFieldValue("customfield_10057", deployIssue.getEstimateTotal());
    }

    private void setEvolutionIssueFields(IssueInputBuilder inputBuilder, EvolutionIssue evolutionIssue) {
        System.out.println("nous sommes dans Evolution");

        // Pour le champs Priorité
        ComplexIssueInputFieldValue priorityFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", evolutionIssue.getPriority())
        );
        inputBuilder.setFieldValue("customfield_10058", Arrays.asList(priorityFieldValue));

        // Pour le champs Nature
        ComplexIssueInputFieldValue natureFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", evolutionIssue.getNature())
        );
        inputBuilder.setFieldValue("customfield_10056", Arrays.asList(natureFieldValue));

        // Pour le champs Prise en compte de l'évolution
        ComplexIssueInputFieldValue recognitionEvolutionFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", evolutionIssue.getRecognitionEvolution())
        );
        inputBuilder.setFieldValue("customfield_10080", Arrays.asList(recognitionEvolutionFieldValue));

        // Pour le champs Format de la demande
        ComplexIssueInputFieldValue formatRequestByFieldValue = new ComplexIssueInputFieldValue(
                Collections.singletonMap("id", evolutionIssue.getRequestFormat())
        );
        inputBuilder.setFieldValue("customfield_10054", Arrays.asList(formatRequestByFieldValue));

        // Pour le champs estimatePure
        inputBuilder.setFieldValue("customfield_10055", evolutionIssue.getEstimatePure());

        // Pour le champs estimateTotal
        inputBuilder.setFieldValue("customfield_10057", evolutionIssue.getEstimateTotal());

    }


    public Issues mapToIssuesModel(Issue issue) throws IOException {
        IssueType issueType = issue.getIssueType();
        System.out.println("Voici issueType: " + issueType.getName());
        if (issueType.getName() != null) {
            String issueTypeName = issueType.getName();
            switch (issueTypeName) {
                case "Bugs":
                    return mapToBugsIssue(issue);
                case "Deployment":
                    return mapToDeployIssue(issue);
                case "Evolution":
                    return mapToEvolutionIssue(issue);
                default:
                    return null;
            }
        }
        return null;
    }


    private BugsIssue mapToBugsIssue(Issue issue) throws IOException {
        BugsIssue bugsIssue = new BugsIssue();

        mapCommonAttributes(issue, bugsIssue);

        //Extraction du Niveau
        String levelField = Objects.requireNonNull(issue.getField("customfield_10079")).getValue().toString();
        JsonNode levelJson = new ObjectMapper().readTree(levelField);
        if (levelJson.isArray() && levelJson.size() > 0) {
            String levelDisplayName = levelJson.get(0).get("value").asText();
            System.out.println("levelDisplayName: " + levelDisplayName);
            bugsIssue.setLevel(levelDisplayName);
        }

        //Extraction de DetectedBy
        String detectedByField = Objects.requireNonNull(issue.getField("customfield_10041")).getValue().toString();
        JsonNode detectedByJson = new ObjectMapper().readTree(detectedByField);
        String detectedByDisplayName = detectedByJson.get("displayName").asText();
        bugsIssue.setDetectedBy(detectedByDisplayName);

        //Extraction du format de la demande
        String requestFormatField = Objects.requireNonNull(issue.getField("customfield_10054")).getValue().toString();
        JsonNode requestFormatFieldJson = new ObjectMapper().readTree(requestFormatField);
        if (requestFormatFieldJson.isArray() && requestFormatFieldJson.size() > 0) {
            String requestFormatDisplayname = requestFormatFieldJson.get(0).get("value").asText();
            System.out.println("requestFormatDisplayname: " + requestFormatDisplayname);
            bugsIssue.setRequestFormat(requestFormatDisplayname);
        }
        return bugsIssue;
    }

    private DeployIssue mapToDeployIssue(Issue issue) throws IOException {
        DeployIssue deployIssue = new DeployIssue();
        mapCommonAttributes(issue, deployIssue);

        //Extraction du champs Priorité
        String priorityField = Objects.requireNonNull(issue.getField("customfield_10058")).getValue().toString();
        JsonNode priorityJson = new ObjectMapper().readTree(priorityField);
        if (priorityJson.isArray() && priorityJson.size() > 0) {
            String priorityDisplayName = priorityJson.get(0).get("value").asText();
            System.out.println("levelDisplayName: " + priorityDisplayName);
            deployIssue.setPriority(priorityDisplayName);
        }


        //Extraction du champs Nature
        String natureField = Objects.requireNonNull(issue.getField("customfield_10056")).getValue().toString();
        JsonNode natureJson = new ObjectMapper().readTree(natureField);
        if (natureJson.isArray() && natureJson.size() > 0) {
            String natureDisplayName = natureJson.get(0).get("value").asText();
            System.out.println("natureDisplayName: " + natureDisplayName);
            deployIssue.setNature(natureDisplayName);
        }


        //Extraction du champs Prise en compte de l'évolution
        String recognitionEvolutionField = Objects.requireNonNull(issue.getField("customfield_10080")).getValue().toString();
        JsonNode recognitionEvolutionJson = new ObjectMapper().readTree(recognitionEvolutionField);
        if (recognitionEvolutionJson.isArray() && recognitionEvolutionJson.size() > 0) {
            String recognitionEvolutionDisplayName = recognitionEvolutionJson.get(0).get("value").asText();
            System.out.println("recognitionEvolutionDisplayName: " + recognitionEvolutionDisplayName);
            deployIssue.setRecognitionEvolution(recognitionEvolutionDisplayName);
        }


        //Extraction de DetectedBy
        String detectedByField = Objects.requireNonNull(issue.getField("customfield_10041")).getValue().toString();
        JsonNode detectedByJson = new ObjectMapper().readTree(detectedByField);
        String detectedByDisplayName = detectedByJson.get("displayName").asText();
        deployIssue.setDetectedBy(detectedByDisplayName);

        deployIssue.setEstimatePure(Double.valueOf(Objects.requireNonNull(issue.getField("customfield_10055")).getValue().toString()));
        deployIssue.setEstimateTotal(Double.valueOf(Objects.requireNonNull(issue.getField("customfield_10057")).getValue().toString()));

        return deployIssue;
    }

    private EvolutionIssue mapToEvolutionIssue(Issue issue) throws IOException {
        EvolutionIssue evolutionIssue = new EvolutionIssue();
        mapCommonAttributes(issue, evolutionIssue);

        //Extraction du champs Priorité
        String priorityField = Objects.requireNonNull(issue.getField("customfield_10058")).getValue().toString();
        JsonNode priorityJson = new ObjectMapper().readTree(priorityField);
        if (priorityJson.isArray() && priorityJson.size() > 0) {
            String priorityDisplayName = priorityJson.get(0).get("value").asText();
            System.out.println("levelDisplayName: " + priorityDisplayName);
            evolutionIssue.setPriority(priorityDisplayName);
        }

        //Extraction du champs Nature
        String natureField = Objects.requireNonNull(issue.getField("customfield_10056")).getValue().toString();
        JsonNode natureJson = new ObjectMapper().readTree(natureField);
        if (natureJson.isArray() && natureJson.size() > 0) {
            String natureDisplayName = natureJson.get(0).get("value").asText();
            System.out.println("natureDisplayName: " + natureDisplayName);
            evolutionIssue.setNature(natureDisplayName);
        }

        //Extraction du champs Prise en compte de l'évolution
        String recognitionEvolutionField = Objects.requireNonNull(issue.getField("customfield_10080")).getValue().toString();
        JsonNode recognitionEvolutionJson = new ObjectMapper().readTree(recognitionEvolutionField);
        if (recognitionEvolutionJson.isArray() && recognitionEvolutionJson.size() > 0) {
            String recognitionEvolutionDisplayName = recognitionEvolutionJson.get(0).get("value").asText();
            System.out.println("recognitionEvolutionDisplayName: " + recognitionEvolutionDisplayName);
            evolutionIssue.setRecognitionEvolution(recognitionEvolutionDisplayName);
        }

        //Extraction du champs format de la demande
        String requestFormatField = Objects.requireNonNull(issue.getField("customfield_10054")).getValue().toString();
        JsonNode requestFormatJson = new ObjectMapper().readTree(requestFormatField);

        System.out.println("###requestFormatJson### " + requestFormatJson);
        if (requestFormatJson.isArray() && requestFormatJson.size() > 0) {
            String requestFormatDisplayName = requestFormatJson.get(0).get("value").asText();
            System.out.println("requestFormatDisplayName: " + requestFormatDisplayName);
            evolutionIssue.setRequestFormat(requestFormatDisplayName);
        }


        evolutionIssue.setEstimatePure(Double.valueOf(Objects.requireNonNull(issue.getField("customfield_10055")).getValue().toString()));
        evolutionIssue.setEstimateTotal(Double.valueOf(Objects.requireNonNull(issue.getField("customfield_10057")).getValue().toString()));

        return evolutionIssue;
    }

    private void mapCommonAttributes(Issue issue, Issues issuesModel) throws IOException {

        issuesModel.setProjectKey(issue.getProject().getName());
        issuesModel.setIssueKey(issue.getKey());
        issuesModel.setIssueType(issue.getIssueType().getName());
        issuesModel.setReporter(Objects.requireNonNull(issue.getReporter()).getDisplayName());
        issuesModel.setEnvironmentUrl(Objects.requireNonNull(issue.getField("customfield_10038")).getValue().toString());

        //Extraction du nom du responsable product CSM
        String managerProductField = Objects.requireNonNull(issue.getField("customfield_10065")).getValue().toString();
        JsonNode managerProductJson = new ObjectMapper().readTree(managerProductField);
        String managerProductDisplayName = managerProductJson.get("displayName").asText();
        issuesModel.setManagerProduct(managerProductDisplayName);


        //Extraction du nom du collaborateur
        String collaboratorField = Objects.requireNonNull(issue.getField("customfield_10052")).getValue().toString();
        JsonNode collaboratorJson = new ObjectMapper().readTree(collaboratorField);
        String collaboratorDisplayName = collaboratorJson.get("displayName").asText();
        issuesModel.setCollaborator(collaboratorDisplayName);

        //Extraction du nom du responsable R&D
        String managerRDField = Objects.requireNonNull(issue.getField("customfield_10066")).getValue().toString();
        JsonNode managerRDJson = new ObjectMapper().readTree(managerRDField);
        String managerRDDisplayName = managerRDJson.get("displayName").asText();
        issuesModel.setManagerRD(managerRDDisplayName);


        issuesModel.setSummary(issue.getSummary());
        issuesModel.setDescription(issue.getDescription());

        String issueLinksString = Objects.requireNonNull(issue.getIssueLinks()).toString();

        System.out.println("@@@@@@@@" + issueLinksString);

        Pattern pattern = Pattern.compile("targetIssueKey=(.*?),");
        Matcher matcher = pattern.matcher(issueLinksString);

        if (matcher.find()) {
            String targetIssueKey = matcher.group(1);
            issuesModel.setIssueLinked(targetIssueKey);
            System.out.println("targetIssueKey: " + targetIssueKey);
        }


        issuesModel.setCommentRecipeKO(Objects.requireNonNull(issue.getField("customfield_10076")).getValue().toString());
        issuesModel.setDateOfRequest(Objects.requireNonNull(issue.getField("customfield_10078")).getValue().toString());
        issuesModel.setDeadlineIntern(Objects.requireNonNull(issue.getField("customfield_10072")).getValue().toString());
        issuesModel.setDeadlineCustomer(Objects.requireNonNull(issue.getField("customfield_10073")).getValue().toString());
        issuesModel.setDateOfRealization(Objects.requireNonNull(issue.getField("customfield_10074")).getValue().toString());
        issuesModel.setDateOfDelivery(Objects.requireNonNull(issue.getField("customfield_10075")).getValue().toString());
        issuesModel.setComments(Objects.requireNonNull(issue.getField("customfield_10050")).getValue().toString());
        issuesModel.setInterestsCollateral(Objects.requireNonNull(issue.getField("customfield_10077")).getValue().toString());

    }


    public List<Issues> getListOfIssue(String projectKey, Integer pageSize, Integer pageNumber) {
        List<Issues> listOfTickets = new ArrayList<>();
        String jqlQuery = "project = " + projectKey;

        try {
            int startIndex = (pageNumber - 1) * pageSize;

            SearchResult searchResult = jiraRestClient.getSearchClient()
                    .searchJql(jqlQuery, pageSize, startIndex, null)
                    .claim();

            Iterable<Issue> resultIssue = searchResult.getIssues();

            for (Issue issue : resultIssue) {
                Issues issuesModel = mapToIssuesModel(issue);
                listOfTickets.add(issuesModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listOfTickets;
    }

    public List<Issues> getListOfIssue(String projectKey) {
        List<Issues> listOfTickets = new ArrayList<>();
        String jqlQuery = "project = " + projectKey;

        try {
            Iterable<Issue> resultIssue = jiraRestClient.getSearchClient().searchJql(jqlQuery).claim().getIssues();

            for (Issue issue : resultIssue) {
                Issues issuesModel = mapToIssuesModel(issue);
                listOfTickets.add(issuesModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listOfTickets;
    }


    @Override
    public void deleteIssue(String issueKey) {
        try {
            jiraRestClient.getIssueClient().deleteIssue(issueKey, true).claim();
            System.out.println("Ticket Jira supprimé avec succès : " + issueKey);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Erreur lors de la suppression du ticket Jira : " + issueKey, e);
        }
    }

    @Override
    public void updateIssue(String issueKey, Issues issues, List<MultipartFile> pictures) throws IOException {
        updateJiraIssue(issueKey, issues, pictures);
    }


    private void updateJiraIssue(String issueKey, Issues issues, List<MultipartFile> pictures) throws IOException {
        IssueInputBuilder inputBuilder = new IssueInputBuilder();
//        inputBuilder.setProjectKey(issues.getProjectKey());

        mapToJiraIssueFields(inputBuilder, issues);

        IssueInput issueInput = inputBuilder.build();

        String jiraBaseUrl = "https://michaelchacha.atlassian.net";
        URI attachmentUri = URI.create(jiraBaseUrl + "/rest/api/2/issue/" + issueKey + "/attachments");

        for (MultipartFile picture : pictures) {
            if (picture != null) {
                InputStream pictureStream = picture.getInputStream();
                jiraRestClient.getIssueClient().addAttachment(attachmentUri, pictureStream, picture.getOriginalFilename());
            }
        }

        try {
            jiraRestClient.getIssueClient().updateIssue(issueKey, issueInput).claim();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mapToJiraIssueFields(IssueInputBuilder inputBuilder, Issues issues) {
        setCommunIssueFiels(inputBuilder, issues);
        if (issues instanceof BugsIssue) {
            setBugsIssueFields(inputBuilder, (BugsIssue) issues);
        } else if (issues instanceof DeployIssue) {
            setDeployIssueFields(inputBuilder, (DeployIssue) issues);
        } else if (issues instanceof EvolutionIssue) {
            setEvolutionIssueFields(inputBuilder, (EvolutionIssue) issues);
        }
    }


    @Override
    public List<ChangelogGroup> getIssueChangelog(String issueKey) {
        Iterable<IssueRestClient.Expandos> expand = List.of(IssueRestClient.Expandos.CHANGELOG);

        System.out.println("EXPAND:  " + expand);
        Issue issue = jiraRestClient.getIssueClient().getIssue(issueKey, expand).claim();

        List<ChangelogGroup> changelogGroups = new ArrayList<>();
        for (ChangelogGroup changelogGroup : Objects.requireNonNull(issue.getChangelog())) {
            changelogGroups.add(changelogGroup);
        }

        return changelogGroups;
    }
}

