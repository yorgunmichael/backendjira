//package qualitadd.MyTicketApp.Impservice;
//
//import jakarta.transaction.Transactional;
//import org.springframework.stereotype.Service;
//import qualitadd.MyTicketApp.entites.EvolutionIssue;
//import qualitadd.MyTicketApp.repository.EvolutionIssueRepository;
//
//@Service
//public class EvolutionIssueService {
//
//    private final EvolutionIssueRepository evolutionIssueRepository;
//
//
//    public EvolutionIssueService(EvolutionIssueRepository evolutionIssueRepository) {
//        this.evolutionIssueRepository = evolutionIssueRepository;
//    }
//
//    @Transactional
//    public void createEvolutionIssue() {
//        EvolutionIssue evolutionIssue = new EvolutionIssue();
//        evolutionIssue.setIssueKey("EVO-001");
//        evolutionIssue.setReporter("Alice");
//        evolutionIssue.setManagerProduct("Manager Product");
//        evolutionIssue.setManagerRD("Manager RD");
//        evolutionIssue.setCollaborator("Collaborator");
//        evolutionIssue.setEnvironmentUrl("http://example.com");
//        evolutionIssue.setSummary("Evolution summary");
//        evolutionIssue.setDescription("Description of the evolution issue");
//        evolutionIssue.setPicture(new byte[]{});
//        evolutionIssue.setIssueLinked("Issue Linked");
//        evolutionIssue.setCommentRecipeKO("Comment Recipe KO");
//        evolutionIssue.setDateOfRequest("2023-07-26");
//        evolutionIssue.setDeadlineIntern("2023-08-15");
//        evolutionIssue.setDeadlineCustomer("2023-08-30");
//        evolutionIssue.setDateOfRealization("2023-08-10");
//        evolutionIssue.setDateOfDelivery("2023-08-25");
//        evolutionIssue.setComments("Comments");
//        evolutionIssue.setInterestsCollateral("Interests Collateral");
//
//        evolutionIssue.setPriority("High");
//        evolutionIssue.setNature("Feature");
//        evolutionIssue.setRecognitionEvolution("Immediate");
//        evolutionIssue.setRequestFormat("Enhancement request");
//        evolutionIssue.setEstimatePure(10);
//        evolutionIssue.setEstimateTotal(20);
//
//        evolutionIssueRepository.save(evolutionIssue);
//    }
//}
