package qualitadd.MyTicketApp.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import qualitadd.MyTicketApp.entites.Issue;
import org.springframework.data.domain.Pageable;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Integer> {
    @Transactional
    @Modifying
    @Query(value = "insert into issue (title, description, type, priority, date_create, status, picture, url, name_project, environment) values (:title,:description, :types, :priority, STR_TO_DATE(:date_create, '%d-%m-%Y'), :status, :picture, :url, :name_project, :env)", nativeQuery = true)
    void createIssue(@Param("title") String title,
                     @Param("description") String description,
                     @Param("types") String types,
                     @Param("priority") String priority,
                     @Param("date_create") String date_create,
                     @Param("status") String status,
                     @Param("picture") String picture,
                     @Param("url") String url,
                     @Param("name_project") String name_project,
                     @Param("env") String env);


    @Transactional
    @Modifying
    @Query("UPDATE Issue SET title = :title, description = :description, issueType = :types, priority = :priority, date_creation = :date_create, status = :status, picture = :picture, url = :url, projectName = :name_project, env = :env WHERE issue_id = :issueId")
    void updateIssue(
            @Param("issueId") Integer issueId,
            @Param("title") String title,
            @Param("description") String description,
            @Param("types") String types,
            @Param("priority") String priority,
            @Param("date_create") String date_create,
            @Param("status") String status,
            @Param("picture") byte[] picture,
            @Param("url") String url,
            @Param("name_project") String name_project,
            @Param("env") String env
    );


    @Query("select i.issue_id, i.title, i.description, i.status, i.priority, i.issueType, i.date_creation, i.url, i.projectName, i.env from Issue i inner join Project p on i.projectName = p.project_name where i.projectName =:name_project")
    Page<Object[]> findByProject(@Param("name_project") String name_project, Pageable pageable);

    @Query("select i.issue_id, i.title, i.description, i.status, i.priority, i.issueType, i.date_creation, i.url, i.projectName, i.env from Issue i inner join Project p on i.projectName = p.project_name where i.projectName =:name_project and i.title =:keyword")
    Page<Object[]> searchIssueByKeyword(@Param("name_project") String name_project, Pageable pageable, @Param("keyword") String keyword);

    @Transactional
    @Modifying
    @Query("delete from Issue WHERE issue_id =:issue_id")
    void deleteIssueById(@Param("issue_id") Integer issue_id);

    @Query("select i.issue_id, i.title, i.description, i.status, i.priority, i.issueType, i.date_creation, i.url, i.projectName, i.env from Issue i inner join Project p on i.projectName = p.project_name where i.projectName =:projectName and (i.status =:statusPos1 or i.status =:statusPos2)")
    Page<Object[]> filterIssueByStatut (@Param("projectName") String projectName, @Param("statusPos1") String statusPos1, @Param("statusPos2") String statusPos2, Pageable pageable);

}

