package qualitadd.MyTicketApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import qualitadd.MyTicketApp.entites.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {
    @Query("select p from Project p inner join p.user u where u.email =:email")
    List<Project> findProjectByEmails(@Param("email") String email);
}
