package qualitadd.MyTicketApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class MyTicketAppApplication  {

	public static void main(String[] args) {

//		ConfigurableApplicationContext context = SpringApplication.run(MyTicketAppApplication.class, args);

//		BugsIssueService bugsIssueService = context.getBean(BugsIssueService.class);
//		bugsIssueService.createBugsIssue();
//
//		EvolutionIssueService evolutionIssueService = context.getBean(EvolutionIssueService.class);
//		evolutionIssueService.createEvolutionIssue();
//
//		context.close();

		try{
		SpringApplication.run(MyTicketAppApplication.class, args);}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
