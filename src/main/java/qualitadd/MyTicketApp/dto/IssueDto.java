package qualitadd.MyTicketApp.dto;

import lombok.Data;

import java.util.Date;

@Data
public class IssueDto {

    private Integer issue_id;

    private String title;

    private String projectName;

    private String description;

    private String issueType;

    private String priority;

    private Date date_creation;

    private String status;

    private byte[] picture;

    private String url;

    private String env;
}
