package qualitadd.MyTicketApp.dto;

import lombok.Data;

@Data
public class ProjectDto {
    private Integer project_id;
    private String project_name;
    private UserDto userDto;

    public void setUser(UserDto userDto) {
        this.userDto = userDto;
    }
}
