package qualitadd.MyTicketApp.dto;

import lombok.Data;


@Data
public class UserDto {
    private Integer id;
    private String name;
    private String email;
    private String password;
    private ProfileDto profileDto;

    public void setProfileDto(ProfileDto profileDto) {
        this.profileDto = profileDto;
    }
}
