package qualitadd.MyTicketApp.dto;

import lombok.Data;

@Data
public class ProfileDto {
    private Integer profile_id;
    private String type_profile;
    private String description;
}
