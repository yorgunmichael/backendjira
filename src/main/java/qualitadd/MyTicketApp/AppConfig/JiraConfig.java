package qualitadd.MyTicketApp.AppConfig;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.JiraRestClientFactory;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;

@Data
@Configuration
@ConfigurationProperties(prefix = "jira")
public class JiraConfig {


    private String url;
    private String username;
    private String password;

    @Bean
    public JiraRestClient jiraRestClient() {
        System.out.println("Creating JiraRestClient...");
        JiraRestClientFactory factory = new AsynchronousJiraRestClientFactory();
        JiraRestClient client =  factory.createWithBasicHttpAuthentication(URI.create(url), username, password);
        System.out.println("JiraRestClient created successfully.");
        return client;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JodaModule());
        return objectMapper;
    }
}

