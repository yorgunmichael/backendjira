package qualitadd.MyTicketApp.entites;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("Evolution")

public class EvolutionIssue extends Issues {

    private String priority;
    private String nature;
    private String recognitionEvolution;
    private String requestFormat;
    private Double estimatePure;
    private Double estimateTotal;

}
