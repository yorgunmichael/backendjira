package qualitadd.MyTicketApp.entites;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "issue")
public class Issue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer issue_id;

    @Column(name = "title")
    private String title;

    @JoinColumn(name = "name_project", referencedColumnName = "project_name")
    @Column(name = "name_project")
    private String projectName;

    @Column(name = "description")
    private String description;

    @Column(name = "type")
    private String issueType;

    @Column(name = "priority")
    private String priority;

    @Column(name = "url")
    private String url;

    @Column(name = "date_create")
    private String date_creation;

    @Column(name = "status")
    private String status;

    @Column(name = "picture")
    private byte[] picture;

    @Column(name = "environment")
    private String env;
}

