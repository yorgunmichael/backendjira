package qualitadd.MyTicketApp.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor

public class DeployIssue extends Issues {

    private String priority;
    private String nature;
    private String recognitionEvolution;
    private String detectedBy;
    private Double estimatePure;
    private Double estimateTotal;

}
