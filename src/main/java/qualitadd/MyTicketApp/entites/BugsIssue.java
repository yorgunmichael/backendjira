package qualitadd.MyTicketApp.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor

public class BugsIssue extends Issues{

    private String level;
    private String detectedBy;
    private String requestFormat;

}
