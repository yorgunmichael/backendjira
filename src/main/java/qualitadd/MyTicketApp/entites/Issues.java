package qualitadd.MyTicketApp.entites;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "issues")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BugsIssue.class, name = "Bugs"),
        @JsonSubTypes.Type(value = DeployIssue.class, name = "Deployment"),
        @JsonSubTypes.Type(value = EvolutionIssue.class, name = "Evolution")
})
public class Issues {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected String projectKey;
    protected String issueKey;
    protected String issueType;
    protected String reporter;
    protected String managerProduct;
    protected String managerRD;
    protected String collaborator;
    protected String environmentUrl;
    protected String summary;
    protected String description;
    //    protected byte[] picture;
//    protected List<byte[]> pictures = new ArrayList<>();
    protected List<MultipartFile> pictures  = new ArrayList<>();
//    protected List<byte[]> pictures  = new ArrayList<>();

    protected String issueLinked;
    protected String commentRecipeKO;
    protected String dateOfRequest;
    protected String deadlineIntern;
    protected String deadlineCustomer;
    protected String dateOfRealization;
    protected String dateOfDelivery;
    protected String comments;
    protected String interestsCollateral;


}
