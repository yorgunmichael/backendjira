package qualitadd.MyTicketApp.MapperModel;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import qualitadd.MyTicketApp.dto.UserDto;
import qualitadd.MyTicketApp.entites.User;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-09-07T14:58:53+0200",
    comments = "version: 1.5.4.Final, compiler: javac, environment: Java 20 (Oracle Corporation)"
)
@Component
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDto usertoDto(User user) {
        if ( user == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setId( user.getId() );
        userDto.setName( user.getName() );
        userDto.setEmail( user.getEmail() );
        userDto.setPassword( user.getPassword() );

        return userDto;
    }

    @Override
    public User DtotoUser(UserDto userDto) {
        if ( userDto == null ) {
            return null;
        }

        User user = new User();

        user.setId( userDto.getId() );
        user.setName( userDto.getName() );
        user.setEmail( userDto.getEmail() );
        user.setPassword( userDto.getPassword() );

        return user;
    }
}
