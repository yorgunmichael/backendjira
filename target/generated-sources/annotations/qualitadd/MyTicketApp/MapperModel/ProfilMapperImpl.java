package qualitadd.MyTicketApp.MapperModel;

import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;
import qualitadd.MyTicketApp.dto.ProfileDto;
import qualitadd.MyTicketApp.entites.Profile;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-09-07T14:58:53+0200",
    comments = "version: 1.5.4.Final, compiler: javac, environment: Java 20 (Oracle Corporation)"
)
@Component
public class ProfilMapperImpl implements ProfilMapper {

    @Override
    public ProfileDto profilToProfileDto(Profile profile) {
        if ( profile == null ) {
            return null;
        }

        ProfileDto profileDto = new ProfileDto();

        profileDto.setProfile_id( profile.getProfile_id() );
        profileDto.setType_profile( profile.getType_profile() );
        profileDto.setDescription( profile.getDescription() );

        return profileDto;
    }

    @Override
    public Profile profilDtoToProfil(ProfileDto profileDto) {
        if ( profileDto == null ) {
            return null;
        }

        Profile profile = new Profile();

        profile.setProfile_id( profileDto.getProfile_id() );
        profile.setType_profile( profileDto.getType_profile() );
        profile.setDescription( profileDto.getDescription() );

        return profile;
    }
}
